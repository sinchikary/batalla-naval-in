package battleship;

public class AirCraft extends Ship{
    /**
     * Constructor <b>AirCraft</b> con un parametro
     * @param location Arreglo de coordenadas donde se ubica el Barco
     * tipo: <b>AirCraft</b>
     */
    public AirCraft(Coordinate[] location){
        super( 8, "AirCraft", location);
    }
}