package battleship;

public class BattleShip extends Ship{
    /**
     * Constructor <b>BattleShip</b> con un parametro
     * @param location Arreglo de coordenadas donde se ubica el Barco
     * tipo: <b>BattleShip</b>
     */
    public BattleShip(Coordinate[] location){
        super( 3, "BattleShip", location);
    }
}