package battleship;

import java.util.Scanner;

public class BattleShipGame {
    static Menus menu=new Menus();
    static Scanner sc = new Scanner(System.in);
    static Player player1 = new Player("Jugador1");
    static Player player2 = new Player("Jugador2");
    static int sizeOcean=20;
    //static boolean tipoUsuario = false;
    
    public static void main(String args[]){
        boolean inicio = Menus.mostarPantallaInicial(player1, player2);
        
        if (inicio) {
            if(player1.getOcean() == null){
                player1.setOcean(20);
                player2.setOcean(20);
                sizeOcean = 20;
            }
            menu.menuIntrucciones();
            mostarEfectoInicio();
            startGame();
            System.out.println("Oceano de: " + player1.getName());
            player1.getOcean().showInvisibleBoard();
            System.out.println("Oceano de: " + player2.getName());
            player2.getOcean().showInvisibleBoard();
        } else {
            exitGame(inicio);
        }  
    }

    /**
     * Crea un efecto de puntos suapensivos antes de iniciar el juego
     */
    public static void mostarEfectoInicio(){        
        for (int i = 0; i < 5; i++) {
            try{
                Thread.sleep(1000);
            }catch(InterruptedException e){
                System.out.println("error");
            }
            System.out.print(".");
        }
        System.out.println("");
    }

    /**
     * Inicia el juego
     */
    public static void startGame() {
        Player player;
        boolean turno = true;
        boolean youWin;
        String disparosStr;
        boolean formatoCorrecto,isInRange;
        sizeOcean = player1.getOcean().sizeBoard;
        if(menu.crearManualmente()){
            menu.instruccionesManuales();
            player1.setOcean(12);
            player2.setOcean(12);
            String coordenadas = sc.nextLine();
            player1.generarBarcoManualmente(player1.getOcean(), coordenadas);
            menu.administrador(player1, player2);
        } else {
            player1.generarAutomaticamente();
            player2.generarAutomaticamente();
        }
        if (menu.getTipoUsuario()) {
            menu.administrador(player1, player2);
        }
        do{
            player = turno? player1 : player2;
            menu.turno(player);
            do{
                disparosStr = sc.nextLine().trim();
                formatoCorrecto = verificarFormato(disparosStr);
                if (formatoCorrecto) {
                    Coordinate[] disparos = leerDisparos(disparosStr);
                    int i = 1;
                    for(Coordinate disparo : disparos){
                        isInRange = isInRange(disparo);
                        if (isInRange) {
                            player.shoot(disparo.getY()-1, disparo.getX()-1);
                        } else {
                            System.out.println("Disparo "+ i +" invalido");
                        }
                        i++;
                    }
                } else {
                    if(disparosStr.equalsIgnoreCase("reiniciar") && preguntar("reiniciar")){
                        restartGame();
                    }else if (disparosStr.equalsIgnoreCase("salir")&& preguntar("salir")) {
                        exitGame(true);
                    }else{
                        System.out.print("Formato incorrecto."
                            + "\nPorfavor ingrese nuevamente las coordenadas: ");
                    }
                }
            }while(!formatoCorrecto);
            System.out.println("-------------------------------------------------------");
            youWin = player.youWin();
            turno = !turno;
        }while(!youWin);
        
        exitGame(true);
    }

    /**
     * Pregunta al usuario si estra seguro de salir o reiniciar el juego
     * @param str cadena de texto que ira concatenada a la pregunta
     * @return <ul>
     *             <li>true:  Si el usuario confirma su desicion</li>
     *             <li>false: Si el usuario declina su desicion</li> 
     *         </ul> 
     */
    public static boolean preguntar(String str){
        menu.menuPreguntar(str);
        String opcion = sc.nextLine().trim();
        switch(opcion){
            case "1":
                return true;

            case "2":
                return false;
                
            default:
                System.out.println("La opcion *" + opcion + "* no es valida");
                System.out.println("Intente de nuevo");
                return preguntar(str);
        }
    }
    
    /**
     * Reinicia el juego
     */
    public static void restartGame() { 
        player1.setOcean(sizeOcean);
        player1.resetData();
        player2.setOcean(sizeOcean);
        player2.resetData();
        startGame();
    }

    /**
     * Revisa si la coordenada de disparo enviada es valida o no 
     * @param disparo coordenada a ser evaluda
     * @return <ul>
     *           <li>true:  Si la <b>coordenada</b> enviada no es null y está
     *                      en el rango deseado</li>
     *           <li>false: Si la <b>coordenada</b> enviada es null o no está
     *                      en el rango deseado</li> 
     *          </ul> 
     */
    public static boolean isInRange(Coordinate disparo){
        if(disparo != null){
            return disparo.getX()>0 && disparo.getX() <= sizeOcean 
                && disparo.getY()>0 && disparo.getY() <= sizeOcean; 
        }
        return false;
    }
    
    /**
     *  Verifica si el formato del parametro enviado es correcto 
     * @param diaparosStr Cadena de texto a evaluar 
     * @return <ul>
     *           <li>true:  Si la cadena de texto tiene el formato correcto</li>
     *           <li>false: Si la cadena de texto no cumple con el 
     *                         formato establecido</li> 
     *          </ul> 
     */
    public static boolean verificarFormato(String diaparosStr){
        String disparosAux = diaparosStr;
        disparosAux = disparosAux.replace(";","").replace(",","");
        char[] charArray = disparosAux.toCharArray();
        for(char elemento: charArray){
            if (!(elemento >= 48 && elemento <= 57)){
                return false;
            }
        }
        return true;
    }
    
    /**
     * Transforma una cadena de texto en un arreglo de Coordenada
     * @param disparosStr cadena de texto
     * @return Un arreglo de coordenadas donde se realizarán disparos 
     */
    public static Coordinate[] leerDisparos(String disparosStr){
        String[] arrayDisparos = disparosStr.split(";");
        Coordinate[] disparos = new Coordinate[arrayDisparos.length];
        String[] coordenadaDisparo;
        int indice = 0; 
        int x, y;
        for(String disparo : arrayDisparos){
            coordenadaDisparo = disparo.split(",");
            if (coordenadaDisparo.length == 2 && indice < 4) {
                x = Integer.valueOf(coordenadaDisparo[0]);
                y = Integer.valueOf(coordenadaDisparo[1]);
                disparos[indice] = new Coordinate(x,y);
            }
            indice++;
        }
        return disparos;
    }
    
    /**
     * Da fin al programa dando un resumen de cada jugador
     * @param inicio<ul>
     *                  <li>true:  Si el juego inició</li>
     *                  <li>false: Si el juego no inició</li> 
     *              </ul> 
     */
    public static void exitGame(boolean inicio) {
        if (inicio) {
            player1.showFinalScreen();
            player2.showFinalScreen();
        }
        System.exit(0);
    }
}