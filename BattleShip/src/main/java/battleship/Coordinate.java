package battleship;

public class Coordinate {
   private int x;
   private int y;
   
    /**
     * Constructor de <b>Coordenada</b> con dos parametros
     * @param x abscisa
     * @param y ordenada
     */
    public Coordinate(int x, int y){
        this.x = x;
        this.y = y;
    }

    /**
     * Retorna el valor del eje <b>x</b> de <b>Coordenada</b>
     * @return abscisa
     */
    public int getX(){
        return x;
    }

    /**
     * Retorna el valor del eje <b>y</b> de <b>Coordenada</b>
     * @return ordenada
     */
    public int getY(){
        return y;
    }

    /**
     * Crea un arreglo de enteros entre las absisas u ordenadas de 
     * <b>Coordenada</b> y <b>finalCoordinate</b> 
     * @param finalCoordinate Cooedenada final
     * @param eje <ul>
      *              <li>true:  Se creara el arreglo entre las abscisas</li>
      *              <li>false: Se creara el arreglo entre las ordenadas</li>
      *          </ul> 
     * @return Arreglo de eteros
     */
    public int[] generateArray(Coordinate finalCoordinate , boolean eje){
        int[] arrayX;
        int ejexyinicial = getXorY(eje);
        int ejexyfinal = finalCoordinate.getXorY(eje);
        if (ejexyinicial == ejexyfinal) {
            arrayX = new int[]{ejexyinicial};
        }else{
            if (ejexyinicial < ejexyfinal) {
                int tam = ejexyfinal - ejexyinicial + 1;
                arrayX = new int[tam];
                for (int i = 0; i < tam; i++) {
                    arrayX[i] = ejexyinicial + i;
                }
            } else {
                int tam = ejexyinicial - ejexyfinal + 1;
                arrayX = new int[tam];
                for (int i = 0; i < tam; i++) {
                    arrayX[i] = ejexyfinal + i;
                }
            }
        }
        return arrayX;
    }
   
    /**
     * Retorna la abscisa u ordenada de <b>Coordenada</b>
     * @param eje <ul>
     *              <li>true:  Para retornar la abscisa</li>
     *              <li>false: Para retornar la ordenada</li>
     *          </ul> 
     * @return Un eje coordenado
     */
    private int getXorY(boolean eje){
        return eje? x: y;
    } 
}
