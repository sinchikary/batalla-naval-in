package battleship;

public class Cruice extends Ship{
    /**
     * Constructor <b>Cruice</b> con un parametro
     * @param location Arreglo de coordenadas donde se ubica el Barco
     * tipo: <b>Cruice</b>
     */
    public Cruice(Coordinate[] location){
        super( 6, "Cruice", location);
    }
}