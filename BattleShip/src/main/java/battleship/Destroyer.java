package battleship;

class Destroyer extends Ship{
    /**
     * Constructor <b>Destroyer</b> con un parametro
     * @param location Arreglo de coordenadas donde se ubica el Barco
     * tipo: <b>Destroyer</b>
     */
    public Destroyer(Coordinate[] location){
        super( 5, "Destroyer", location);
    }
}