package battleship;

import java.util.Scanner;

public class Menus {

    static Scanner sc=new Scanner(System.in);
    static Menus s=new Menus();
    static boolean tipoUsuario = false;
    static boolean crearManualmente = false;

    public void menuSecundario() {
        System.out.println("-------------BATTLE SHIP GAME-------------");
        System.out.println("Menu Ociones:");
        System.out.println("        1.- Cambiar nombre jugador1");
        System.out.println("        2.- Cambiar nombre jugador2");
        System.out.println("        3.- Cambiar Dificultadad");
        System.out.println("        4.- Cambiar tipo de Usuario");
        System.out.println("        5.- Crear barcos manualmente" + 
                         "\n        6.- Volver al menu principal ");
        System.out.println("------------------------------------------");

    }

    public void menuInicio() {
        System.out.println("-------------BATTLE SHIP GAME-------------");
        System.out.println("Menu principal:");
        System.out.println("        1.- Iniciar Juego");
        System.out.println("        2.- Opciones");
        System.out.println("        3.- Salir");
        System.out.println("------------------------------------------");

    }

    public void menuDificultades() {
        System.out.println("-------------BATTLE SHIP GAME-------------");
        System.out.println("Dificultades:");
        System.out.println("        1.- Facil    (12 x 12)");
        System.out.println("        2.- Medio    (16 x 16)");
        System.out.println("        3.- Dificil  (20 x 20)");
        System.out.println("------------------------------------------");
    }

    public void menuIntrucciones() {
        System.out.println("----------------INSTRUCCIONES PREVIAS----------------");
        System.out.println(" * El formato de disparo es el siguiente:");
        System.out.println("   ->  x1,y1;x2,y2;x3,y3;x4,y4  <-");
        System.out.println("      Donde:");
        System.out.println("       - x1, x2, x3, x4, y1, y2, y3, y4");
        System.out.println("         son numeros enteros positivos");
        System.out.println("       - ',' es el delimitador entre 'x' e 'y'");
        System.out.println("       - ';' es el delimitador entre cada coordenada");
        System.out.println(" * Cualquier otro formato es invalido");
        System.out.println(" * Se pueden realizar 4 disparos por turno o");
        System.out.println("   menos si así lo desea el jugador");
        System.out.println(" * Si los numeros ingresados estan fuera de rango");
        System.out.println("   el disparo será invalido");
        System.out.println("-----------------------------------------------------\niniciando partida");

    }

    public void menuUsuario() {
        System.out.println("-------------BATTLE SHIP GAME-------------");
        System.out.println("Tipo de usuario: ");
        System.out.println("        1.- Usuario Administrador");
        System.out.println("        2.- Usuario Normal");
        System.out.println("------------------------------------------");
    }

    public void menuPreguntar(String str) {
        System.out.println("-------------BATTLE SHIP GAME-------------");
        System.out.println("¿Seguro que desea " + str + "?");
        System.out.println("        1.- Si");
        System.out.println("        2.- No");
        System.out.println("------------------------------------------");
    }

    public void turno(Player player) {
        System.out.println("-------------------------------------------------------");
        System.out.println("Es turno de: " + player.getName().toUpperCase());
        System.out.println("            Para salir del juego ingrese: salir");
        System.out.println("            Para reiniciar del juego ingrese: reiniciar");
        player.showData();
        System.out.print("Ingrese las coordenadas de disparo: ");
    }

    public void administrador(Player player1, Player player2) {
        System.out.println("----Tablero de: " + player1.getName().toUpperCase() + "--------------------");
        player1.getOcean().showInvisibleBoard();
        System.out.println("----Tablero de: " + player2.getName().toUpperCase() + "--------------------");
        player2.getOcean().showInvisibleBoard();
        System.out.println("-------------------------------------------------------");
    }

    /**
     * Muestra en consola las dificultades disponibles al usuario
     */

    public static void desplegarMenuDificultades(Player player1, Player player2, int sizeOcean) {

        s.menuDificultades();
        String opcion = sc.nextLine().trim();  
        switch(opcion){
            case "1":
                player1.setOcean(12);
                player2.setOcean(12);
                sizeOcean = 12;
                break;
            case "2":
                player1.setOcean(16);
                player2.setOcean(16);
                sizeOcean = 16;
                break;
            case "3":
                player1.setOcean(20);
                player2.setOcean(20);
                sizeOcean = 20;
                break;
            default:
                System.out.println("La opcion *" + opcion + "* no existe");
                System.out.println("Intente de nuevo");
                desplegarMenuDificultades(player1 ,player2, sizeOcean);                
        }
    }

     /**
     * Muestra en consola el menu principal del juego
     * @return <ul>
     *              <li>true:  Si el usuario desea iniciar el juego</li>
     *              <li>false: Si el usuario desea salir del juego</li>
     *          </ul>
     */
    static boolean mostarPantallaInicial(Player player1, Player player2) {

        s.menuInicio();
        String opcion = sc.nextLine().trim();
        switch (opcion) {
            case "1":
                return true;
            case "2":
                desplegarMenuOpciones(player1, player2);
                return mostarPantallaInicial(player1, player2);
            case "3":
                return false;
            default:
                System.out.println("La opcion *" + opcion + "* no existe");
                System.out.println("Intente de nuevo");
                return mostarPantallaInicial(player1, player2);
        }
    }
 
    public static boolean mostrarMenuOpciones(Player player1, Player player2) {
		return false;
    }
    public void instruccionesManuales(){
        System.out.println("Para generar un barco manualmente escriba el "+
            "tamaño del barco,la orientacion y las coordenadas "+
            "\n en este formato : \n 3;3;(1,1)  ");
        System.out.println("para la dirección números del 0-3"
            +"\n 0: Izquierda. 1:Derecha 2:Arriba 3:Abajo ");
    }
    
    /**
     * Muestra en consola el menu de opciones al usuario
     */

    static void desplegarMenuOpciones(Player player1, Player player2) {

        s.menuSecundario();
        String opcion = sc.nextLine().trim();
        switch (opcion) {
            case "1":
                System.out.print("Ingrese nombre jugador1: ");
                String name1 = sc.nextLine();
                player1.setName(name1);
                desplegarMenuOpciones(player1, player2);
                break;
            case "2":
                System.out.print("Ingrese nombre jugador2: ");
                String name2 = sc.nextLine();
                player2.setName(name2);
                desplegarMenuOpciones(player1, player2);
                break;
            case "3":
                desplegarMenuDificultades(player1, player2, 20);
                desplegarMenuOpciones(player1, player2);
                break;
            case "4":
                desplegarTiposDeUsuario();
                desplegarMenuOpciones(player1, player2);
                break;
            case "5":
                crearManualmente = true;
                break;    
            case "6":
                break;
            default:
                System.out.println("La opcion *" + opcion + "* no existe");
                System.out.println("Intente de nuevo");
                desplegarMenuOpciones(player1, player2);
        }
    }
    static void desplegarTiposDeUsuario(){
        s.menuUsuario();
        String opcion = sc.nextLine().trim();
        switch(opcion){
            case "1":
                tipoUsuario = true;
                break;
            case "2":
                tipoUsuario = false;
                break;
            default:
                System.out.println("La opcion *" + opcion + "* no es valida");
                System.out.println("Intente de nuevo");
                desplegarTiposDeUsuario();
        }
    }
    boolean getTipoUsuario(){
        return tipoUsuario;
    }
    boolean crearManualmente(){
        return crearManualmente;
    }	
}