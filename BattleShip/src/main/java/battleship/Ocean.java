package battleship;

import java.util.Random;

public class Ocean {

    private char[][] board;
    private char[][] visibleBoard;
    public Ship[] ships;
    private Random random = new Random();
    int sizeBoard;

    /**
     * Constructor con un parametro
     * @param size Tamanio del tablero
     */
    public Ocean(int size) {
        sizeBoard = size;
        board = new char[sizeBoard][sizeBoard];
        visibleBoard = new char[sizeBoard][sizeBoard];
        ships = new Ship[11];
        generateBoard();
        /*
         1 AirCraft     (8)
         1 Cruice       (6)
         2 Destroyer    (5)
         3 BattleShip   (3)
         4 Frigate      (2)
         */  
    }

    /**
     * Cambia el estado de un espacio del oceano cuando disparan a ese espacio
     * @param x abscisa
     * @param y ordenada
     * @return <ul>
     *              <li>0: el disparo impactó en el agua</li>
     *              <li>1: el disparo impactó en un barco</li>
     *              <li>2: el disparo impactó y hundió un barco</li>
     *          </ul> 
     */

    public int changStsBox(int x, int y) {
        int indicador = 0;
        switch (board[x][y]) {
            case '.':
                visibleBoard[x][y] = 'X';
                break;
            case '|':
                visibleBoard[x][y] = 'X';
                break;
            case '0':
                if (visibleBoard[x][y] != '-' && visibleBoard[x][y] != '0') {
                    visibleBoard[x][y] = '-';
                    indicador = changStsShip(x, y) ? 2 : 1;
                    return indicador;
                }
                break;
        }
        return indicador;
    }
    
    /**
     * Cambia el estado del barco impactado
     * @param x abscisa
     * @param y ordenada
     * @return <ul>
     *              <li>true: el barco se dañó</li>
     *              <li>false: el barco se hundió</li>
     *          </ul>
     */

    private boolean changStsShip(int x, int y) {
        for (Ship ship : ships) {
            for (Coordinate c1 : ship.location) {
                if (x == c1.getX() && y == c1.getY()) {
                    ship.hurt();
                    if (ship.getStatus().equals("Hundido")) {
                        chnagStsBoxToSuken(ship.location);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Cambia el estado de un conjunto de casillas a hundido 
     * @param coordinates coordenadas del barco hundido
     */

    private void chnagStsBoxToSuken(Coordinate[] coordinates) {
        for (Coordinate coordinate : coordinates) {
            visibleBoard[coordinate.getX()][coordinate.getY()] = '*';
        }
    }

     /**
     * Muestra en consola el tablero con el registro de los disparos 
     */
    public void showVisibleBoard() {
        showBoard(visibleBoard);
    }

    /**
     * Muestra en consola el tablero con barcos
     */
    public void showInvisibleBoard() {
        showBoard(board);
    }

    public void addToShipRegister(int numero, Ship ship){
        ships[numero] = ship;
    }

    /**
     * Genera aleatoriamente la ubicacion de cada barco del juego y los coloca 
     * en el tablero
     */

    public void generateShips() {
        int numBarco = 0;
        while (numBarco < 11) {
            if (numBarco < 1) {
                //System.out.println("AirCraft creado");
                Coordinate[] location;
                location = generateLocation(8);
                setShipInOcean(location);
                setSpaceForShip(location[0],location[7],8); 

                ships[numBarco] = new Ship(8, "Aircraft", location);
            } else if (numBarco < 2) {
                //System.out.println("Cruice creado");
                Coordinate[] location;
                location = generateLocation(6);
                setShipInOcean(location);
                setSpaceForShip(location[0],location[5],6);       //en prueba 

                ships[numBarco] = new Ship(6, "Cruice", location);
            } else if (numBarco < 4) {
                //System.out.println("Destroyer creado");
                Coordinate[] location;
                location = generateLocation(5);
                setShipInOcean(location);
                setSpaceForShip(location[0],location[4],5); 
                
                ships[numBarco] = new Ship(5,"destroyer", location);
            } else if (numBarco < 7) {
                //System.out.println("BattleShip creado");
                Coordinate[] location;
                location = generateLocation(3);
                setShipInOcean(location);
                setSpaceForShip(location[0],location[2],3); 

                ships[numBarco] = new Ship(3, "BattleShip", location);
            } else {
                //System.out.println("Frigate creado");
                Coordinate[] location;
                location = generateLocation(2);
                setShipInOcean(location);
                setSpaceForShip(location[0],location[1],2); 

                ships[numBarco] = new Ship(2, "Frigate", location);
            }
            numBarco++;
        }
    }


    /**
     * Muestra en consola el tablero con barcos o el tablero con el registro 
     * de los disparos
     * @param board tablero con los barcos o el tablero con el registro de 
     * los disparos
     */

    private void showBoard(char[][] board) {
        System.out.print("  ");
        for (int i = 1; i <= sizeBoard; i++) System.out.print((((int) (i / 10)) == 0 ? "  " : " ") + i);
        System.out.println();
        for (int i = 1; i <= sizeBoard; i++) {
            System.out.print(i);
            System.out.print(((int) (i / 10)) == 0 ? "   " : "  ");
            for (int j = 0; j < sizeBoard; j++) {
                if (board[i-1][j] == '|') {
                    System.out.print( ".  ");
                } else {
                    System.out.print(board[i-1][j] + "  ");
                }
                
            }
            System.out.println("");
        }
    }

   
    /**
     * Llena los tableros con un '.' en cada espacio
     */
    private void generateBoard() {
        for (int i = 0; i < sizeBoard; i++) {
            for (int j = 0; j < sizeBoard; j++) {
                board[i][j] = '.';
                visibleBoard[i][j] = '.';
            }
        }
    }

    /**
     * Genera aleatoriamente un areglo de coordenadas 
     * @param size tamanio del arreglo a generar
     * @return Arreglo de coordenadas
     */

    private Coordinate[] generateLocation(int size) {
        int direction = generateDirection();
        Coordinate initialCoordinate = generateCoordinate();
        Coordinate finalCoordinate = generateFinalCoordonate(size, direction, initialCoordinate);
        Coordinate[] coordinates;
        if (isInRange(finalCoordinate) && hasNecessarySpace(initialCoordinate, finalCoordinate)) {
            coordinates = new Coordinate[size];
            for (int i = 0; i < size; i++) {
                switch (direction) {
                    case 0:
                        coordinates[i] = new Coordinate(initialCoordinate.getX() + i, initialCoordinate.getY());
                        break;
                    case 1:
                        coordinates[i] = new Coordinate(initialCoordinate.getX() - i, initialCoordinate.getY());
                        break;
                    case 2:
                        coordinates[i] = new Coordinate(initialCoordinate.getX(), initialCoordinate.getY() + i);
                        break;
                    case 3:
                        coordinates[i] = new Coordinate(initialCoordinate.getX(), initialCoordinate.getY() - i);
                        break;
                }
            }
        } else {
            return generateLocation(size);
        }
        return coordinates;
    }

    /**
     * Genera una coordenada final 
     * @param size distancia de la coordenada inicial a la final
     * @param direction <ul>
     *                      <li>0: Hacia la derecha</li>
     *                      <li>1: Hacia la izquierda</li>
     *                      <li>2: Hacia arriba</li>
     *                      <li>3: Hacia abajo</li>
     *                  </ul>
     * @param coordinate Coordenada inicial
     * @return Coordenada final
     */

    private Coordinate generateFinalCoordonate(int size, int direction, Coordinate coordinate) {
        size--;
        Coordinate finalCoordinate;

        switch (direction) {
            case 0:
                finalCoordinate = new Coordinate(coordinate.getX() + size, coordinate.getY());
                break;
            case 1:
                finalCoordinate = new Coordinate(coordinate.getX() - size, coordinate.getY());
                break;
            case 2:
                finalCoordinate = new Coordinate(coordinate.getX(), coordinate.getY() + size);
                break;
            case 3:
                finalCoordinate = new Coordinate(coordinate.getX(), coordinate.getY() - size);
                break;
            default:
                return null;
        }
        return finalCoordinate;
    }

    /**
     * Verifica si la coordenada final generada esta dentro del rango del tablero
     * @param finalCoordinate coordenada a verificar
     * @return <ul>
     *              <li>true: La coordenada esta en el rango</li>
     *              <li>false: La coordenada no esta en el rango</li>
     *          </ul>
     */

    private boolean isInRange(Coordinate finalCoordinate) {
        int finalX = finalCoordinate.getX();
        int finalY = finalCoordinate.getY();
        return finalX >= 0 && finalX < sizeBoard && finalY >= 0 && finalY < sizeBoard;
    }

    /**
     * Verifica si se tiene el espacio necesario para crear un barco
     * @param initialCoordinate Coordenada inicial
     * @param finalCoordinate Coordenada final
     * @return <ul>
     *              <li>true: Si se tiene el espacio necesario</li>
     *              <li>false: No se tiene el espacio necesario</li>
     *          </ul>
     */

    private boolean hasNecessarySpace(Coordinate initialCoordinate, Coordinate finalCoordinate) {
        int[] arrayX = initialCoordinate.generateArray(finalCoordinate, true);
        int[] arrayY = initialCoordinate.generateArray(finalCoordinate, false);

        if (arrayX.length == 1) {
            for (int i = 0; i < arrayY.length; i++) {
                if (board[arrayX[0]][arrayY[i]] != '.') {
                    return false;
                }
            }
        } else {
            for (int i = 0; i < arrayX.length; i++) {
                if (board[arrayX[i]][arrayY[0]] != '.') {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Genera randomicamente un numero
     * @return <ul>
     *             <li>0: Derecha</li>
     *             <li>1: Izquierda</li>
     *             <li>2: Arriba</li>
     *             <li>3: Abajo</li>
     *         </ul>
     */
    private int generateDirection() {
        return random.nextInt(4);
    }

    /**
     * Genera randomicamente una coordenada dentro del rango establecido
     * @return Coordenada aleatoria
     */

    private Coordinate generateCoordinate() {
        int x = random.nextInt(sizeBoard);
        int y = random.nextInt(sizeBoard);
        return new Coordinate(x, y);
    }

    /**
     * Añade un barco en la ubicacion ingresada
     * @param coordinates Ubicacion del barco
     */

    protected void setShipInOcean(Coordinate[] coordinates) {
        for (int i = 0; i < coordinates.length; i++) {
            board[coordinates[i].getX()][coordinates[i].getY()] = '0';
        }
    }

    /**
     * Añade '|' al rededor del barco para que ningun otro barco pueda ubicarse 
     * junto a el
     * @param initialCoordinate coordenada inicial del barco
     * @param finalCoordinate coordenada final del barco 
     * @param size tamanio del barco
     */

    private void setSpaceForShip(Coordinate initialCoordinate, Coordinate finalCoordinate, int size) {
        size--;
        int[] arrayX =  initialCoordinate.generateArray(finalCoordinate, true);
        int[] arrayY =  initialCoordinate.generateArray(finalCoordinate, false);
        
        boolean orientation = (arrayX.length == 1);
        int[] parXorY = orientation? new int[]{arrayX[0] - 1,arrayX[0] + 1}:
        new int[]{arrayY[0] - 1,arrayY[0] + 1};
        int sup = orientation? arrayY[size] + 1:arrayX[size] + 1;
        int inf = orientation?arrayY[0] - 1:arrayX[0] - 1;
        
        for(int xOrY : parXorY){
            for (int i = inf; i <= sup; i++) {
                if ( i>=0 && i<sizeBoard && xOrY>=0 && xOrY<sizeBoard) {
                    if(orientation){
                        board[xOrY][i] = '|';
                    }else{
                        board[i][xOrY] = '|';
                    }
                }
            }
        }
    }
    
    public void setbarcoManualmente(int size, int direction, Coordinate coordinate){
        int ships =0;
        Coordinate[] coordinates = new Coordinate[size];
        if(isInRange(coordinate) && size <= 8 && direction <=4 && direction>-1 && ships<11) {
            Coordinate coordenada = generateFinalCoordonate(size, direction, coordinate);
            if (isInRange(coordenada) && hasNecessarySpace(coordinate,coordenada)) {
                for (int i = 0; i < size; i++) {
                    switch (direction) {
                        case 0:
                            coordinates[i] = new Coordinate(coordinate.getX() + i, coordinate.getY());
                            break;
                        case 1:
                            coordinates[i] = new Coordinate(coordinate.getX() - i, coordinate.getY());
                            break;
                        case 2:
                            coordinates[i] = new Coordinate(coordinate.getX(), coordinate.getY() + i);
                            break;
                        case 3:
                            coordinates[i] = new Coordinate(coordinate.getX(), coordinate.getY() - i);
                            break;
                    }
                }
            }
        }
        setShipInOcean(coordinates);
    }
}
