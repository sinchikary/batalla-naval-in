package battleship;

public class Player {
    private String name;
    private int score, sunkenShips, remainingToSinkShips;
    private Ocean oceanAdversario;

    /**
     * Constructor con un parametro
     * @param name Nombe del Jugador
     */
    public Player (String name) {
        this.name = name;
        score = 0;
        sunkenShips = 0;
        remainingToSinkShips = 11; 
    }

    public void generarAutomaticamente(){
        oceanAdversario.generateShips();
    }
    
    /**
     * Asigna un oceano nuevo a Jugador
     * @param size Tamanio del oceano nuevo
     */
    public void setOcean(int size){
        oceanAdversario = new Ocean(size);
    }

    /**
     * Retorna el atributo <b>Oceano</b> de <b>Jugador</b>
     * @return Oceano 
     */
    public Ocean getOcean(){
        return oceanAdversario;
    }
    
    public void increaseScore() {
        score++;
    }

    public int getScore() {
        return score;
    }
    
    /**
     * Realiza el disparo a una casilla del Oceano
     * @param x abscisa 
     * @param y ordenada
     */
    public void shoot(int x, int y) {
        int indicador = oceanAdversario.changStsBox(x, y); 
        if( indicador > 0){
            increaseScore();
            if(indicador > 1 ){
                remainingToSinkShips--;
                sunkenShips++;
            }
        }
    }

    /**
     * Reinicia los datos del <b>Jugador</b>
     */
    public void resetData(){
        score = 0;
        sunkenShips = 0;
        remainingToSinkShips = 11;
    }
    
    /**
     * Asigna un nuevo nombre a <b>Jugador</b>
     * @param name Nuevo nombre
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Retorna el nombre del <b>Jugador</b>
     * @return Nombre del Jugador
     */
    public String getName() {
        return name;
    }

    /**
     * Muestra en consola los datos actuales del Jugador
     */
    public void showData(){
        System.out.println("Puntaje acumulado: " + score);
        System.out.println("Barcos restantes a hundir: " + remainingToSinkShips);
        System.out.println("Barcos hundidos: " + sunkenShips);
        oceanAdversario.showVisibleBoard();
    }

    /**
     * Muestra en consola el resumen final del Jugador
     */
    public void showFinalScreen() {
        System.out.println("-------Resumen del Jugador: "+ name.toUpperCase() + "----------");
        System.out.println(youWin()?"           GANASTE!":
                "NO LOGRASTE DERRIBAR TODOS LOS BARCOS");
        System.out.println("Tu puntaje es: " + score);
        System.out.println("Barcos hundidos: " + sunkenShips);
        System.out.println("Barcos restantes a hundir: " + remainingToSinkShips);
    }

    /**
     * Revisa si el Jugador ha ganado o no la partida
     * @return  <ul>
     *              <li>true:  Si el Jugaador ha ganado la partida</li>
     *              <li>false: Si el Jugaador no ha ganado la partida</li>
     *          </ul> 
     */
    public boolean youWin() { 
        if (sunkenShips == 11) {
            return true;
        } 
        return false;
    }

    public void generarBarcoManualmente(Ocean oceano, String manuales){
        
        String instrucciones=manuales.trim().replaceAll(";","");
        
        int x = 0;
        int y = 0;
        int size = instrucciones.charAt(0)-'0';
        int direction = instrucciones.charAt(1)-'0';
        for(int i=0 ; i<instrucciones.length(); i++){
            if(instrucciones.charAt(i) == ','){
              x = instrucciones.charAt(i-1)-'0';
              y = instrucciones.charAt(i+1)-'0'; 
            }
        }
        Coordinate coordinate = new Coordinate(x,y);
        Coordinate[] coordinates = new Coordinate[size];
        coordinates = ingresarCoordenadas(size, direction, coordinate);
        oceano.addToShipRegister(0, addShips(size, coordinates));
        oceano.setShipInOcean(coordinates);
    }

    public Coordinate[] ingresarCoordenadas(int size,int direction,Coordinate coordinate){
        Coordinate[] coordinates= new Coordinate[size];
        Coordinate coordenada;
        for(int i = 0; i<size;i++){
            switch (direction) {
                case 0:
                    coordenada = new Coordinate(coordinate.getX(),coordinate.getY()-1);
                    coordinates[i] = coordenada;
                    break;
                case 1:
                    coordenada = new Coordinate(coordinate.getX(),coordinate.getY()+i);
                    coordinates[i] = coordenada;
                    break;
                case 2:
                    coordenada= new Coordinate(coordinate.getX(),coordinate.getY()-i);
                    coordinates[i] = coordenada;
                    break;
                case 3:
                    coordenada = new Coordinate(coordinate.getX()+i,coordinate.getY());
                    coordinates[i] = coordenada;
                    break;    
                default:
                    break;
            }
        }
        return coordinates;
    }

    private Ship addShips(int size, Coordinate[] location){
        String name="0";
        switch (size) {
            case 8:
                name = "Aircraft";
                break;
            case 6:
                name = "Cruice";
                break;
            case 5:
                name = "Destroyer";
                break;
            case 3:
                name = "Battleship";
                break;    
            case 2:
                name = "Frigate";
                break;
            default:
                break;
        }
        Ship ship = new Ship(size, name, location);
        return ship;
    }
}