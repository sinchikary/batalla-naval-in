package battleship;

public class Ship {

    protected int size;
    protected String type;
    protected String status;
    protected int lifePoints;
    protected Coordinate[] location;

    /*enum status{
        A_FLOTE,
        DANIADO,
        HUNDIDO
    }*/
    
    /**
     * Constructor con 3 parametros
     * @param size Tamanio del BarcoArreglo de Coordenadas donde se ubica el Barco
     * @param type Tipo del Barco
     * @param location Arreglo de coordenadas donde se ubica el Barco
     */
    public Ship(int size, String type, Coordinate[] location) {
        this.size = size;
        this.type = type;
        this.location = location;
        lifePoints = size;
        status = "A flote";
    }

    /**
     * Baja los puntos de vida y actualiza el estado del Barco
     */
    public void hurt() {
        lifePoints--;
        updateShip();
    }

    /**
     * Retorna el estado del Barco
     * @return estado actual del Barco
     */
    public String getStatus() {
        return status;
    }

    /**
     * Retorna los puntos de vida del Barco
     * @return Puntos actuales de vida
     */
    public int getLifePoints() {
        return lifePoints;
    }

    /**
     * Cambia el estado del Barco a Daniado
     */
    private void changeStatusToDamaged() {
        status = "Daniado";
    }

    /**
     * Cambia el estado del Barco a Hundido y muestra en consola que tipo
     * de Barco ha sido hundido  
     */
    private void changeStatusToSuken() {
        status = "Hundido";
        System.out.println("barco: " + type + " esta HUNDIDO");
    }

    /**
     * Actualiza el estado del barco segun los puntos actuales de vida
     */
    private void updateShip() {
        if (lifePoints == 0) {
            changeStatusToSuken();
        } else if (lifePoints < size) {
            changeStatusToDamaged();
        }
    }
}