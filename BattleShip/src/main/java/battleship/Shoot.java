package battleship;
class Shoot{
    private int x;
    private int y;
    private String coordenada,palabra;
    private char[][] registro;

    public Shoot(String coordenada){
        this.coordenada=coordenada;
        palabra="";
    }
    
    public void armarRegistro(){
        registro=new char[10][10];
        for (int i = 0; i < registro.length; i++) {
            for (int j = 0; j < registro.length; j++) {
                registro[i][j]='.';
            }  
        }
    }
    public void nuevaPosicion(String coordenada){
        this.coordenada=coordenada;
    }

    public void Disparo(){
        if(coordenada.length()>2){
            System.out.println("coordenada incorrecta");
        }else if(coordenada.length()!=0) {
            palabra=coordenada.toLowerCase();
            x=(int)palabra.charAt(0)-(int)'a';
            y=(int)palabra.charAt(1)-(int)'1';
            Coordinate coor=new Coordinate(x, y);
            registro[coor.getX()][coor.getY()]='X';
        }
    }

    public void  imprimirRegistro(){
        for (int i = 0; i < registro.length; i++) {
            for (int j = 0; j < registro.length; j++) {
                System.out.print(" "+registro[i][j]);
            }
            System.out.println("");
        }
    }

}