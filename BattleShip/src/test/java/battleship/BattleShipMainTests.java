package battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;



public class BattleShipMainTests {
    @Test
    public void testverifyFormato() {
        String disparosStr = "1,2;3,4;5,5";
        boolean actual = BattleShipGame.verificarFormato(disparosStr);
        assertEquals(true, actual);
    }
    @Test
    public void testshootsOUTofrange(){
        Coordinate corrdinate = new Coordinate(21, 21);
        assertEquals(false,BattleShipGame.isInRange(corrdinate));
    }
    @Test
    public void testshootINrange(){
        Player player1 = new Player("s");
        player1.setOcean(12);
        Coordinate coordinate = new Coordinate(10, 10);
        assertEquals(true,BattleShipGame.isInRange(coordinate));
    }
    
    
}