package battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;



public class CoordinateTests {
    @Test
    public void testreturnX() {
        Coordinate coordinate = new Coordinate(3, 2);
        assertEquals(3, coordinate.getX());
    }
    @Test
    public void testreturnY() {
        Coordinate coordinate = new Coordinate(3, 2);
        assertEquals(2, coordinate.getY());
    }
}