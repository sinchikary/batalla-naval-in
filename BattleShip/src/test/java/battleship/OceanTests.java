package battleship;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;




public class OceanTests {
    @Test
    public void testverifyChngStatusBox() {
        // . = agua = X = 0
        // ® = barco = - = barco herido = 1
        // ® = * = barco hundido = 2
        Ocean ocean = new Ocean(20);
        
        assertEquals(0, ocean.changStsBox(10, 10)); 
    }

    // Verificamos si imprime el board del jugador
    // y el board donde se ven las posiciones de los barcos
    /*public static void main(String[] args) {
        Ocean ocean = new Ocean(20);
        ocean.showVisibleBoard();
        System.out.println("-------------------------------------------------------------------");
        ocean.showInvisibleBoard();
    }*/
    @Test
    public void testShipRegister(){
        Ocean ocean = new Ocean(4);
        Coordinate d1 = new Coordinate(1, 1);
        Coordinate d2 = new Coordinate(2, 1);
        
        Coordinate[] d3 = new Coordinate[]{d1, d2}; 
        Ship ship = new Ship(2, "frigate", d3);
        ocean.addToShipRegister(0, ship);
        
    }
}