package battleship;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;



public class PlayerTests {
    @Test
    public void testreturnOcean() {
        Player player1 = new Player("");
        player1.setOcean(20);
        Ocean ocean = player1.getOcean();
        Ocean expected = ocean;
        assertEquals(expected, ocean);
    }
    @Test
    public void testreturnName() {
        Player player1 = new Player("Cristelh");
        assertThat(player1.getName(), is(player1.getName()));
    }
    @Test
    public void testsetName() {
        Player player1 = new Player("");
        player1.setName("Pablo");
        assertEquals("Pablo", player1.getName());
    }
    @Test
    public void testincreaseScore() {
        Player player1 = new Player("");
        player1.increaseScore();
        assertEquals(1, player1.getScore());
    }
    @Test
    public void testverifyShoot() {
        Player player2 = new Player("");
        player2.setOcean(12);
        player2.shoot(10, 10);
        assertEquals(1, player2.getScore());
    }
    @Test
    public void testverifyRestartGame() {
        Player player1 = new Player("");
        player1.increaseScore();//score = 1
        player1.resetData();
        assertEquals(0, player1.getScore());
    }
    @Test
    public void testCoordinateArray(){
        Player player1 = new Player("player1");
        Coordinate coor = new Coordinate(1,1);
        Coordinate coor1 = new Coordinate(2,1);
        Coordinate coor2 = new Coordinate(3,1);
        Coordinate[] coordinates = new Coordinate[3];
        coordinates = player1.ingresarCoordenadas(3,3,coor);
        int[] actualX = new int[3];
        int[] actualY = new int[3];
        for (int i = 0; i < coordinates.length; i++) {
            actualX[i] = coordinates[i].getX();
            actualY[i] = coordinates[i].getY();
        }
        int[] ExpectedX = new int[]{coor.getX(),coor1.getX(),coor2.getX()};
        int[] ExpectedY = new int[]{coor.getY(),coor1.getY(),coor2.getY()};
        assertThat(ExpectedX, is(actualX));
        assertThat(ExpectedY, is(actualY));
    }
    public static void main(String[] args) {
        Player player1= new Player("sa");
        Coordinate coordinate = new Coordinate(5,5);
        Coordinate[] coordinates = new Coordinate[4];
        coordinates = player1.ingresarCoordenadas(4, 1, coordinate);
        for (int i = 0; i < coordinates.length; i++) {
            System.out.println(coordinates[i].getX());
        }
    }
    
    
}