package battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;



public class ShipTests {
    @Test
    public void testreturnStatus() {
        Coordinate[] location = new Coordinate[3];
        Ship airCraft = new Ship(8, "Aircraft", location);
        assertEquals("A flote", airCraft.getStatus());
        airCraft.hurt();
        assertEquals("Daniado", airCraft.getStatus());
    }
    @Test
    public void testhurtShip() {
        Coordinate[] location = new Coordinate[3];
        Ship airCraft = new Ship(8, "aircraft", location);
        //puntos de vida = tamaño del barco = 8
        airCraft.hurt();
        assertEquals(7, airCraft.getLifePoints());
    }
    @Test
    public void testSunkingShip(){
        Coordinate[] location = new Coordinate[2];
        Ship frigate =new Ship(2, "frigate", location);
        frigate.hurt();
        frigate.hurt();
        assertEquals("Hundido",frigate.status);
    }
    public void testShip(){
        
    }
}